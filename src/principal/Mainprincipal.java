package principal;

import java.sql.Connection;
import java.util.Scanner;

import gestionNotes.Enseignants;
import gestionNotes.Etudiants;
import gestionNotes.Manager;

public class Mainprincipal {


	private static Scanner scan;

	public static void main(String[] args) {
			
		String URL = "jdbc:mysql://localhost:3306/db_notes";
		String ClassName = "con.mysql.jdbc.Driver";
		connexion con = new connexion(URL, "root", "", ClassName);
        Connection connexion = con.connectToDB();
        if(connexion == null) {
        	System.out.println("Connexion timed out");
        }else {
        	System.out.println("CONNECTED");
        }
	
		int choix;
		scan = new Scanner(System.in);
		
		System.out.println(" -----------------------  Bienvenu(e) sur votre Gestionnaire de notes -----------------------\n");
									
		System.out.println("Veuillez faire un choix d'action pour continuer");
									
		System.out.println("1-Action sur les Etudiants \n2-Action sur les Enseignants \n3-Actions sur les UE\n4-Page Etudiant \n5-Page Enseignant \n");
		
		choix = scan.nextInt();
		
		switch(choix) {
		case 1:
			Manager admin = new Manager();
			admin.ManageEntityNamed("Etudiants");
			break;
		case 2:
			Manager administrator = new Manager();
			administrator.ManageEntityNamed("Enseignants");
			break;
		case 3:
			Manager mananger = new Manager();
			mananger.ManageEntityNamed("UE");
			break;
		case 4:
			int action;
			Etudiants etudiant = new Etudiants();
		    Scanner sc = new Scanner(System.in);
			System.out.println("-------------------------------- Bienvenu(e) sur la page etudiant  ----------------\n");
			System.out.println("Que voulez vous faire: \n");
			System.out.println("1-Consultation \n2-Reclamation\n");
			action = sc.nextInt();
			etudiant.actions(action);
			break;
		case 5:
			Enseignants enseignant = new Enseignants();
			enseignant.Manage("Etudiants");
			break;
			
		default:
			
			System.out.println("Aucune action choisie!");
		}
	}

}
