package gestionNotes;

public class Reclamation {
	private float noteConteste;
	
	private Etudiants etudiant;
	private UE ue;
	
	
	public float getNoteConteste() {
		return noteConteste;
	}
	public void setNoteConteste(float noteConteste) {
		this.noteConteste = noteConteste;
	}
	public Etudiants getEtudiant() {
		return etudiant;
	}
	public void setEtudiant(Etudiants etudiant) {
		this.etudiant = etudiant;
	}
	public UE getUe() {
		return ue;
	}
	public void setUe(UE ue) {
		this.ue = ue;
	}
	

}
