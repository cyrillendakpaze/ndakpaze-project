package gestionNotes;

import java.util.Scanner;

import services.EnseignantService;
import services.EtudiantService;
import services.NoteService;
import services.ReclamService;
import services.UeService;

public class Etudiants extends Users{
	
	private Scanner sc;

	public void actions(int action) {
		EtudiantService etudserv = new EtudiantService();
		NoteService Noteser = new NoteService();
		int id;
		String Username;
		String Password;
		sc = new Scanner(System.in);
		switch(action) {
		case 1:
			System.out.println("---------------------- Bienvenu(e) cher Etudiant --------");
			System.out.println("------------------------ Veuillez vous authentifier pour continuer---------------------\n");
			System.out.println("Votre identifiant : ");
			id = sc.nextInt();
			System.out.println("Ok entrez votre nom d'utilisateur :");
			Username = sc.nextLine();
			Username = sc.nextLine();
			System.out.println("Ok indiquer votre mot de passe : ");
			Password = sc.nextLine();
			
			Etudiants etud = etudserv.verifUser(id);
			
			if(etud.getUsername().equals(Username) && etud.getPassword().equals(Password)) {
				
				System.out.println("Connected:  \t  Profil "+etud.getUsername()+"\n");
				System.out.println(" ---------Affichage des notes et les ue ----------\n");
				Noteser.getAll(id);	
			}else {
				System.out.println("Authentification failed");
			}
			
			break;
		case 2:
			String mail;
			System.out.println("---------------------- Bienvenu(e) cher Etudiant --------");
			System.out.println("------------------------ Veuillez vous authentifier pour continuer---------------------\n");
			System.out.println("Votre identifiant svp (un entier entre 0 et plus) : ");
			id = sc.nextInt();
			System.out.println("Ok renseigner votre nom d'utilisateur :");
			Username = sc.nextLine();
			Username = sc.nextLine();
			System.out.println("Ok indiquer votre mot de passe : ");
			Password = sc.nextLine();
			
			Etudiants etud1 = etudserv.verifUser(id);
			
			if((etud1.getUsername().equals(Username) && etud1.getPassword().equals(Password))) {
				System.out.println("Connected:  \t  Profil "+etud1.getUsername()+"\n");
				System.out.println(" --------- Formulaire de Reclamation  ----------\n");
				Reclamation rec = new Reclamation();
				UeService uesr = new UeService();
				EnseignantService enserv = new EnseignantService();
				Etudiants etu = etudserv.getById(id);
				rec.setEtudiant(etu);
				
				System.out.println("Indiquer  l'unite d'enseignement dans lequel vous souhaiter reclamer: ");
				String Libelle = sc.nextLine();
				UE u = uesr.getByNom(Libelle);
				rec.setUe(u);
				
				
				System.out.println(" veuillez indiquer la note que vous contester");
				float note = sc.nextFloat();
				rec.setNoteConteste(note);
				
				mail = "Bonjour Monsieur Je viens vers vous faire une reclamation ,Vous avez attribuer une note dont je suis pas sur [Nom :"+etu.getNom()+", Prenom:"+etu.getPrenom()+",Note:"+note+"]";
				
				ReclamService rc = new ReclamService();
				rc.Addreclamation(rec,enserv.getenseignantId(u.getCodeUE()),mail);

			}
			
			break;
		default:
			System.out.println("Aucune action encours");
		}
	}

}
